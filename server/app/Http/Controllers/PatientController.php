<?php

namespace App\Http\Controllers;

use App\Patient;
use App\Photo;
use Illuminate\Http\Request;
use Image;

class PatientController extends Controller
{
    //
    public function add(Request $request){
        $patient = new Patient();
        $patient->name      = $request->input('name');
        $patient->tc        = $request->input('tc');
        $patient->phone     = $request->input('phone');
        $patient->birthYear = $request->input('birthYear');
        $patient->note      = $request->input('note');

        if($patient->save()){
            return response()->json(["msg" => true]);
        }else{
            return response()->json(["msg" => false]);
        }
    }   

    public function lists(){
        return response()->json(Patient::orderBy('id','DESC')->get(), 200);
    } 

    public function detail($id){
        $patient = Patient::find($id);
        return response()->json($patient, 200);
    } 

    public function photoUpload($id){
        $file = request()->file('image');
        
        $ext  = $file->getClientOriginalExtension();
        $fileName = rand(11111,99999).'.'.$ext;
        $destinationPath = public_path('images/');
        $destinationPathResize = public_path('images/thumb');

        $photo = new Photo();
        $photo->patient_id  = $id;
        $photo->name        = $fileName;
        if($photo->save()){
            Image::make($file->getRealPath())->resize(250,250)->save($destinationPathResize.'/'.$fileName, 80);
            $file->move($destinationPath, $fileName);      
        }
        return response('uploaded', 200);
    } 

    public function photoList($id){
        $photoList = Photo::where('patient_id', $id)->get();
        return response()->json($photoList, 200);
    }    

}
