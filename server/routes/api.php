<?php
use App\User;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('test', function(Request $request){
    return response(User::find(1), 200);
});



Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'v1', 'middleware'=> 'auth:api'], function(){
    Route::group(['prefix'=>'patient'], function(){
        Route::get('list', 'PatientController@lists');
        Route::get('detail/{id}', 'PatientController@detail');   
        Route::post('add', 'PatientController@add');
        Route::post('photo/{id}', 'PatientController@photoUpload');
        Route::get('photo/list/{id}', 'PatientController@photoList');
    });
});