import Vue from 'vue'
import {newPatientUrl, listPatientUrl, listPatientDetailUrl, patientPhotoListUrl, getHeader} from './../../config'
const state = {
    patientList: {},
    photoList: {},    
    patient: {},
    isPatientListMenu: ''
}

const mutations = {
    SET_PATIENT_LIST (state, patientList) {
        state.patientList = patientList
    },
    SET_NEW_PATIENT (state, newPatient) {
        state.newPatient = newPatient   
    },
    SET_DETAIL_PATIENT (state, patient) {
        state.patient = patient   
    },
    SET_PATIENT_PHOTO_LIST (state, photoList) {
        state.photoList = photoList
    },    
    SET_PATIENT_LIST_MENU (state, isPatientListMenu) {
        state.isPatientListMenu = isPatientListMenu   
    }         
}

const actions = {
    setPatientList: ({commit}, patientList) => {
        return Vue.http.get(listPatientUrl, {headers: getHeader()})
            .then(res => {
                commit('SET_PATIENT_LIST', res.body)                
            })        
    },
    setNewPatient: ({commit}, newPatient) => {
        return Vue.http.post(newPatientUrl, newPatient, {headers: getHeader()})
            .then(res => {
               commit('SET_NEW_PATIENT', res.body.msg)
                if(res.body.msg == true){
                    UIkit.modal("#modal-example").hide();
                    UIkit.notification("Kayıt başarılı", "success");
                }else{
                    UIkit.notification("HATA! Lütfen tekrar deneyiniz.", "danger");   
                }                    
            })          
    },
    setPatientDetail: ({commit}, id) => {
        return Vue.http.get(listPatientDetailUrl + id, {headers: getHeader()})
            .then(res => {
                commit('SET_DETAIL_PATIENT', res.body)                
            })        
    },
    setPatientPhotoList: ({commit}, patient_id) => {
         return Vue.http.get(patientPhotoListUrl+patient_id, {headers: getHeader()})
            .then(res => {
                commit('SET_PATIENT_PHOTO_LIST', res.body)            
            })         
    },
    setPatientListMenu: ({commit}, bool) => {
        commit('SET_PATIENT_LIST_MENU', bool)
    }    
}

export default {
    state, mutations, actions
}