// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'
import store from './store'

Vue.config.productionTip = false

import App from './App'

import Login from './pages/login.vue'
import Dashboard from './pages/dashboard.vue'
import PatientDetail from './components/patient/detail.vue'


Vue.use(VueRouter)
Vue.use(VueResource)

Vue.component('app', App)

const routes = [
  {path: '/', component: Login, name: 'home'},
  {path: '/dashboard', component: Dashboard, name: 'dashboard', meta: {requiresAuth: true}},
  {path: '/patient/:id', component: PatientDetail, name: 'PatientDetail', meta: {requiresAuth: true}, query: {isPatientListMenu: true}}
]

const router = new VueRouter({
  mode : 'history',
  base:__dirname,
  routes
})

router.beforeEach((to, from, next) => {
      if(to.meta.requiresAuth){
        const authUser = JSON.parse(window.localStorage.getItem('authUser'))
        if(authUser && authUser.access_token){
          next()
        }else{
          next({name:'home'})
        }
      }
      next()
})

new Vue ({
  router, store
}).$mount('#app')
