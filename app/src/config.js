export const apiDomain      = 'http://10.30.33.111:8000/'
export const loginUrl       = apiDomain + 'oauth/token'
export const userUrl        = apiDomain + 'api/user'
export const api            = apiDomain + 'api/v1/'
export const newPatientUrl  = api + 'patient/add' 
export const listPatientUrl  = api + 'patient/list' 
export const listPatientDetailUrl   = api + 'patient/detail/' 
export const patientPhotoUploadUrl  = api + 'patient/photo/' 
export const patientPhotoListUrl    = api + 'patient/photo/list/'
export const patientPhotoUrl        = apiDomain + 'images/'
export const patientPhotoUrlThumb   = apiDomain + 'images/thumb/'

export const getHeader = function () {
    const tokenData = JSON.parse(window.localStorage.getItem('authUser'))
    const headers = {
        'Accept' : 'application/json',
        'Authorization' : 'Bearer ' + tokenData.access_token
    }
    return headers
}